package test

import (
	"encoding/json"
	"fmt"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/terraform"
	test_structure "github.com/gruntwork-io/terratest/modules/test-structure"
	"os"

	//	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
	"time"
)

func TestIntEKSSetup(t *testing.T) {
	t.Parallel()
	stage := test_structure.RunTestStage

	defer stage(t, "destroy_network", func() {
		destroyNetwork(t)
	})
	stage(t, "create_network", func() {
		createNetwork(t)
	})


	defer stage(t, "destroy_eks", func() {
		destroyEks(t)
	})
	stage(t, "create_eks", func() {
		region := test_structure.LoadString(t, "/tmp", "region")
		vpcId := test_structure.LoadString(t, "/tmp", "vpc_id")
		subnetIds := test_structure.LoadString(t, "/tmp", "subnet_ids")
		createEks(t, region, vpcId, subnetIds)
	})

}

func destroyEks(t *testing.T) {
	opts := test_structure.LoadTerraformOptions(t, "/tmp/eks")
	terraform.Destroy(t, opts)
}

func createEks(t *testing.T, region string, vpcId string, subnetIds string) {

	rand.Seed(time.Now().UnixNano())
	os.Setenv("AWS_DEFAULT_REGION", region)
	randVal := rand.Intn(99)
	testClusterName := fmt.Sprint("eks_test", randVal)
	accessing_computer_ip := "97.120.179.43"
	terraformOptions := &terraform.Options{
		TerraformDir: "../modules/eks",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			//"user": testUser,
			"cluster_name": testClusterName,
			"vpc_id": vpcId,
			"region": region,
			"accessing_computer_ip": accessing_computer_ip,
			"app_subnet_ids": subnetIds,

		},
	}
	test_structure.SaveTerraformOptions(t, "/tmp/eks", terraformOptions)


	terraform.InitAndApplyE(t, terraformOptions)
	//print(terraformOptions.Vars)
	//return terraformOptions.Vars.region
}

func destroyNetwork(t *testing.T) {
	opts := test_structure.LoadTerraformOptions(t, "/tmp")
	terraform.Destroy(t, opts)
}
func createNetwork(t *testing.T) {
	subnetCount := 2
	/*
	 * When doing basic network testing, it's common to receive VpcLimitExceeded
	 * `Error creating VPC: VpcLimitExceeded: The maximum number of VPCs has been reached.`
	 * Therefore, catch it and retry so we hit another region.
	 */
	// Pick a random AWS region to test in. This helps ensure your code works in all regions.
	// I know us-east-1 works so using that.
	var stableRegions = []string{"us-east-1"}


	//r := rand.New(time.Now().UnixNano())
	rand.Seed(time.Now().UnixNano())

	randVal := rand.Intn(99)
	testUser := fmt.Sprint("atest", randVal)
	//testClusterName := fmt.Sprint("network_test", randVal)

	vpcOptions := &terraform.Options{
		TerraformDir: "../modules/network",

		RetryableTerraformErrors: map[string]string{
			".*VpcLimitExceeded.*": "Error creating VPC: VpcLimitExceeded: The maximum number of VPCs has been reached. Try another region.",
			".*AddressLimitExceeded.*": "Error: Error creating EIP: AddressLimitExceeded: The maximum number of addresses has been reached.",
			".*NatGatewayLimitExceeded.*": "Error creating NAT Gateway: NatGatewayLimitExceeded: Performing this operation would exceed the limit of 5 NAT gateways",
		},
		MaxRetries: 5,
		TimeBetweenRetries: 3*time.Second,
		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"user": testUser,
			"vpc_name": testUser,
			"region": aws.GetRandomStableRegion(t, stableRegions, nil),
			"subnet_count": subnetCount,
		},
	}
	test_structure.SaveTerraformOptions(t, "/tmp", vpcOptions)
	//defer terraform.Destroy(t, terraformOptions)
	region := vpcOptions.Vars["region"].(string)
	os.Setenv("AWS_DEFAULT_REGION", region)
	terraform.InitAndApply(t, vpcOptions)
	test_structure.SaveString(t, "/tmp", "region", region)
	vpcId := terraform.Output(t, vpcOptions, "vpc_id")
	test_structure.SaveString(t, "/tmp", "vpc_id", vpcId)
	appSubnetIds := terraform.OutputList(t, vpcOptions, "app_subnet_ids")
	subnetIds, _ := json.Marshal(appSubnetIds)
	test_structure.SaveString(t, "/tmp", "subnet_ids", string(subnetIds))

}
