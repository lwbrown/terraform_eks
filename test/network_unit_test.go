package test

import (
	"fmt"

	"math/rand"
	"os"
	"testing"
	"time"
	"encoding/json"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/terraform"
)


func getJsonMap(m map[string]interface{}, key string) map[string]interface{} {
	raw := m[key]
	sub, ok := raw.(map[string]interface{})
	if !ok {
		return nil
	}
	return sub
}
// We should test VPC, Subnets, Route Tables and Gateways
// https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform_part2/
func TestUnitNetworkSetup(t *testing.T) {
	t.Parallel()
	subnetCount := 2
	/*
	 * When doing basic network testing, it's common to receive VpcLimitExceeded
	 * `Error creating VPC: VpcLimitExceeded: The maximum number of VPCs has been reached.`
	 * Therefore, catch it and retry so we hit another region.
	 */

	// Pick a random AWS region to test in. This helps ensure your code works in all regions.
	awsRegion := aws.GetRandomStableRegion(t, nil, nil)
	os.Setenv("AWS_DEFAULT_REGION", awsRegion)

	//r := rand.New(time.Now().UnixNano())
	rand.Seed(time.Now().UnixNano())

	randVal := rand.Intn(99)
	testUser := fmt.Sprint("network_test", randVal)
	//testClusterName := fmt.Sprint("network_test", randVal)

	tfOptions := &terraform.Options{
		TerraformDir: "../modules/network",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"user": testUser,
			"vpc_name": testUser,
			//"cluster-name": testClusterName, // I don't think this is necessary here.
			"region": awsRegion,
			"subnet_count": subnetCount,
		},
	}
	// init and plan
	tfPlanOutput := "terraform.tfplan"
	terraform.Init(t, tfOptions)
	terraform.RunTerraformCommand(t, tfOptions, terraform.FormatArgs(tfOptions, "plan", "-out="+tfPlanOutput)...)
	tfOptions.Vars = nil

	// read the plan as json
	jsonplan, err := terraform.RunTerraformCommandAndGetStdoutE(t, tfOptions, terraform.FormatArgs(tfOptions, "show", "-json", tfPlanOutput)...)
	jsonMap := make(map[string]interface{})
	err = json.Unmarshal([]byte(jsonplan), &jsonMap)
	if err != nil {
		panic(err)
	}
	plannedValues := getJsonMap(jsonMap, "planned_values")
	rootModule := getJsonMap(plannedValues, "root_module")
	print(rootModule)
}
