package test

import (
	"fmt"
	ttaws "github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/retry"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"log"
	"net"

	//"log"
	"math/rand"
	"encoding/json"
	//"net"
	"os"
	"testing"
	"time"
)

func getJsonMap(m map[string]interface{}, key string) map[string]interface{} {
	raw := m[key]
	sub, ok := raw.(map[string]interface{})
	if !ok {
		return nil
	}
	return sub
}

// Get preferred outbound ip of this machine
func getOutboundIP() net.IP {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}

func TestUnitSecurityGroup(t *testing.T) {
	t.Parallel()
	/*
	 * Create VPC
	 */
	retryDescription := "Return value after 5 retries with expected error, match regex"
	retryableErrors := map[string]string{
		".*one default VPC in region.*": "Expected to find one default VPC in region _______ but found 0.",
	}
	maxRetries := 5
	retry.DoWithRetryableErrorsE(t, retryDescription, retryableErrors, maxRetries, 1*time.Millisecond, func() (string, error) {
		awsRegion := ttaws.GetRandomStableRegion(t, nil, nil)
		os.Setenv("AWS_DEFAULT_REGION", awsRegion)
		rand.Seed(time.Now().UnixNano())
		// For Testing, we are going to just use the default VPC
		awsVPC := ttaws.GetDefaultVpc(t, awsRegion)
		rand.Seed(time.Now().UnixNano())

		randVal := rand.Intn(99)
		testClusterName := fmt.Sprint("test", randVal)
		testIP := getOutboundIP()
		tfOptions := &terraform.Options{
			TerraformDir: "../modules/security_groups",

			// Variables to pass to our Terraform code using -var options
			Vars: map[string]interface{}{
				"cluster_name":          testClusterName,
				"accessing_computer_ip": testIP.String(),
				"vpc_id":                awsVPC.Id,
				"region":                awsRegion,
			},
		}

		// init and plan
		tfPlanOutput := "terraform.tfplan"
		terraform.Init(t, tfOptions)
		terraform.RunTerraformCommand(t, tfOptions, terraform.FormatArgs(tfOptions, "plan", "-out="+tfPlanOutput)...)
		tfOptions.Vars = nil

		// read the plan as json
		jsonplan, err := terraform.RunTerraformCommandAndGetStdoutE(t, tfOptions, terraform.FormatArgs(tfOptions, "show", "-json", tfPlanOutput)...)
		jsonMap := make(map[string]interface{})
		err = json.Unmarshal([]byte(jsonplan), &jsonMap)
		if err != nil{
			panic(err)
		}
		plannedValues := getJsonMap(jsonMap, "planned_values")
		rootModule := getJsonMap(plannedValues, "root_module")
		print(rootModule)
		return jsonplan, err
	})
}
