#!/bin/bash

mkdir $1
touch $1/main.tf
touch $1/variables.tf
touch $1/output.tf
touch $1/$1_test.go
cd $1
go mod init $1