
# src: https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform-part-iv
resource "aws_eks_cluster" "eks_cluster" {
  name            = var.cluster_name
  role_arn        = aws_iam_role.eks_master.arn

  vpc_config {
    security_group_ids = [aws_security_group.sg_eks_master.id]
    subnet_ids         = var.app_subnet_ids
  }

  depends_on = [
    aws_iam_role_policy_attachment.tf-cluster-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.tf-cluster-AmazonEKSServicePolicy,
  ]
}
