########################################################################################
# setup provider for kubernetes
# src: https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform-part-iv

//data "external" "aws_iam_authenticator" {
//  program = ["sh", "--debug", "-c", "aws-iam-authenticator token -i ${var.cluster_name} | jq -r -c .status"]
//}

data "aws_eks_cluster_auth" "cluster_auth" {
  name = var.cluster_name
}

provider "kubernetes" {
  host                      = aws_eks_cluster.eks_cluster.endpoint
  cluster_ca_certificate    = base64decode(aws_eks_cluster.eks_cluster.certificate_authority.0.data)
  #token                     = data.external.aws_iam_authenticator.result.token
  token = data.aws_eks_cluster_auth.cluster_auth.token
  load_config_file          = false
}

# Allow worker nodes to join cluster via config map
# This step is running before the master actually starts. Unfortunately, sg_eks_master returns a success before the url is available.
resource "kubernetes_config_map" "aws_auth" {
  metadata {
    name = "aws-auth"
    namespace = "kube-system"
  }
  data = {
    mapRoles = <<EOF
- rolearn: ${aws_iam_role.eks_node.arn}
  username: system:node:{{EC2PrivateDNSName}}
  groups:
    - system:bootstrappers
    - system:nodes
EOF
  }
  depends_on = [
    aws_eks_cluster.eks_cluster,aws_autoscaling_group.sg_eks_master  ]
}
