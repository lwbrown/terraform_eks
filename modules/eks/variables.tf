
# src: https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform-part-iv
variable "vpc_id" {
  type = string
}

variable "accessing_computer_ip" {
  type = string
  description = "Public IP of the computer accessing the cluster via kubectl or browser."
}

variable "region" {
  type = string
  description = "WIP: List of used aws_regions. Should be a single one, might not be used at all."
}
// I'm leaving this out for now as I don't need ssh access to every node.
// Will add it when we have everything else working.
//variable "keypair-name" {
//  type = string
//  description = "Name of the keypair declared in AWS IAM, used to connect into your instances via SSH."
//}

variable "app_subnet_ids" {
  type    = list(string)
  description = "App Subnet IDs"
}

variable "cluster_name" {
  type = string
}
