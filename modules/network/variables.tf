# REQUIRED FOR VPC
variable "user" {
  description = "Who are you?"
}
variable "region" {
  description = "AWS region"
}
variable "vpc_name" {
  description = "name of AWS cluster"
}
variable "subnet_count" {
  description = "Number of subnets to spin up"
}
